// Load updates
if (Drupal.jsEnabled) {
  $(document).ready(function () {
    loadupdates();
    setInterval("loadupdates()", Drupal.settings.livecoverage.refresh_rate);
  });
}
function loadupdates() {
  var timestamp = new Date().getTime();
  uniqueupdatefile = Drupal.settings.livecoverage.updatefile + '?' + timestamp;
  $.get(uniqueupdatefile, function(txt) {
    $("#entry-list").html(txt)  
  });
}
